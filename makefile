all:
	cd src && $(MAKE)
	mv src/gcalc .

clean:
	rm gcalc
	cd src && $(MAKE) clean
