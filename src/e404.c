#include "io.h"
#include "e404.h"

void error_404(){
    say("Google");
    say("404. (that's an error.)");
    say("The requested URL /calc could not be found on this server. (That's all we know.)");
}
