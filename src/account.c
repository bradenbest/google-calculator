#include <stdio.h>

#include "io.h"
#include "account.h"

void account(){
    char herring[1000] = {0};
    char *responses[] = {
        "Invalid username or password",
        "Internal Server error",
        "Umm...",
        "Your account was deleted because you violated DMCA on YouTube",
        "No. No usernames. You need to log in with your ID. That's something like EAX342784523CD. Check your Google+ page."
    };
    int responses_len = sizeof(responses) / sizeof(responses[0]);

    say("Please provide account details to continue...");
    printf("Username: ");
    scanf("%s", herring);
    printf("Password: ");
    scanf("%s", herring);
    say("Please wait...");
    wait(10000);
    say(responses[rand() % responses_len]);
}
