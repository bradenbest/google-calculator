#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "io.h"
#include "incomplete-calculation.h"
#include "internal-server-error.h"
#include "an-error-occurred.h"
#include "something-went-wrong.h"
#include "maintenance.h"
#include "complex.h"
#include "account.h"
#include "e404.h"

const void (*results[])() = {
    incomplete_calculation,
    internal_server_error,
    an_error_occurred,
    something_went_wrong,
    maintenance,
    too_complex,
    account,
    error_404,
};

const int results_len = sizeof(results) / sizeof(results[0]);

int main(){
    int a, b;

    srand(time(0));
    printf("Google Calculator v1.04\nGive me something to calculate\n>>> ");
    scanf("%i%i", &a, &b);
    say("Please wait...");
    wait(5000);
    results[rand() % results_len]();
}
