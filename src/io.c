#include <stdio.h>
#include <stdlib.h>

#include "io.h"

void say(char *msg){
    printf("%s\n", msg);
}

void wait(int ms){
    usleep(1000 * ms);
}
