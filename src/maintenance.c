#include "io.h"
#include "maintenance.h"

void maintenance(){
    say("Hang on. We're performing maintenance...");
    while(1);
}
