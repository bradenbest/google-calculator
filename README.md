# Google Calculator
## A calculator designed with Google's philosophy of how programs should function

### Usage

Just run `make` in a terminal to compile from source and run `./gcalc`

    $ git clone https://gitlab.com/bradenbest/google-calculator.git
    $ cd google-calculator/
    $ make
    $ ./gcalc